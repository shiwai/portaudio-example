# portaudio-example

## Description
WAVファイルをportaudioを利用して再生します    
mono/stereo, 8/16/24bit, 44100/48000Hzのフレームレートに対応している（はず）

## Build
```
$ g++ main.cpp -o portaudio-example -lportaudio
```

## Run
```
$ portaudio-example {path-to-your-wav-file}
```
