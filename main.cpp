#include <stdio.h>
#include <iostream>
#include <unistd.h>

#include "portaudio.h"

typedef struct {
    int sampleRate;
    int channelNum;
    int bytesPerSample;
    int8_t* wavData;
    int wavLength;
} WavFileFormat;

typedef struct {
    int currentIndex;
    WavFileFormat* wavInfo;
} PlayBackInfo;

WavFileFormat* loadWavData(char path[]) {
    WavFileFormat* wavFileFormat = new WavFileFormat();

    errno = 0;
    FILE* fp = fopen(path, "r");

    if (fp == NULL) {
        std::cout << "failed to open file: " << path << std::endl;
        std::cout << strerror(errno) << std::endl;
        return NULL;
    }

    char* riff = new char[4];
    uint32_t chunkSize;
    char* format = new char[4];
    char* fmt = new char[4];
    uint32_t byteSize;
    uint16_t soundFormat;
    uint16_t channelNum;
    uint32_t sampleRate;
    uint32_t avgSamplePerSec;
    uint16_t blockSize;
    uint16_t bitSize;
    char* subChunkId = new char[4];
    uint32_t subChunkSize;

    int readLen = fread(riff, 4, 1, fp);
    readLen = fread(&chunkSize, 4, 1, fp);
    readLen = fread(format, 4, 1, fp);
    readLen = fread(fmt, 4, 1, fp);
    readLen = fread(&byteSize, 4, 1, fp);
    readLen = fread(&soundFormat, 2, 1, fp);
    readLen = fread(&channelNum, 2, 1, fp);
    readLen = fread(&sampleRate, 4, 1, fp);
    readLen = fread(&avgSamplePerSec, 4, 1, fp);
    readLen = fread(&blockSize, 2, 1, fp);
    readLen = fread(&bitSize, 2, 1, fp);

    if (soundFormat != 1) {
        // TODO: Read Extend Formats If soundFormat is not Linear PCM
    }

    bool dataChunkReady = false;
    do {
        readLen = fread(subChunkId, 4, 1, fp);
        readLen = fread(&subChunkSize, 4, 1, fp);
        if (strcmp(subChunkId, "data") != 0) {
            fseek(fp, subChunkSize, SEEK_CUR);
        } else {
            dataChunkReady = true;
        }
    } while (dataChunkReady == false);

    std::cout << "======= basic data ======="<< std::endl;
    std::cout << riff << std::endl;
    std::cout << chunkSize << std::endl;
    std::cout << format << std::endl;
    std::cout << fmt << std::endl;
    std::cout << byteSize << std::endl;
    std::cout << soundFormat << std::endl;
    std::cout << channelNum << std::endl;
    std::cout << sampleRate << std::endl;
    std::cout << avgSamplePerSec << std::endl;
    std::cout << blockSize << std::endl;
    std::cout << bitSize << std::endl;

    std::cout << "======= extended data ======="<< std::endl;
    std::cout << subChunkId << std::endl;
    std::cout << subChunkSize << std::endl;

    std::cout << "======= wave data format ======="<< std::endl;
    int bytesPerData = bitSize / 8;

    std::cout << "======= wave data ======="<< std::endl;

    int8_t* wavData = new int8_t[subChunkSize];

    int result = fread(wavData, 1, subChunkSize, fp);


    fclose(fp);

    delete[] format;
    delete[] riff;
    delete[] fmt;
    delete[] subChunkId;

    wavFileFormat->sampleRate = sampleRate;
    wavFileFormat->bytesPerSample = bytesPerData;
    wavFileFormat->channelNum = channelNum;
    wavFileFormat->wavData = wavData;
    wavFileFormat->wavLength = subChunkSize;

    return wavFileFormat;
}

int portAudioPlayCallback(const void *input, void *output,
                           unsigned long frameCount,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData) {
    PlayBackInfo* playBackInfo = (PlayBackInfo*)userData;
    WavFileFormat* wavInfo = playBackInfo->wavInfo;

    if (playBackInfo->currentIndex >= wavInfo->wavLength) {
        return paComplete;
    }

    int readBlockSize = frameCount * wavInfo->channelNum * wavInfo->bytesPerSample; // channel * bytesPerFrame

    int remainsData = wavInfo->wavLength - playBackInfo->currentIndex;
    int copySize = remainsData >= readBlockSize?  readBlockSize : remainsData;

    int8_t* cursor = wavInfo->wavData;
    cursor = cursor + (playBackInfo->currentIndex);

    // same as below
    // memcpy(output, cursor, copySize);

    int8_t * outputPtr = (int8_t*)output;

    for (int i = 0; i < copySize; i += wavInfo->channelNum * wavInfo->bytesPerSample) {
        // L/R
        for (int channel = 0; channel < wavInfo->channelNum; channel++) {
            // bytesPerSample (if format is pa16Int, need to write twice)
            for (int byteSize = 0; byteSize < wavInfo->bytesPerSample; byteSize++) {
                *(outputPtr++) = cursor[i + (channel * wavInfo->bytesPerSample) + byteSize];
            }
        }
    }

    playBackInfo->currentIndex += readBlockSize;

    return paContinue;
}

void playByPortAudio(PlayBackInfo playBackInfo) {
    PaError error;
    error = Pa_Initialize();
    if (error != paNoError) {
        std::cout << Pa_GetErrorText(error) << std::endl;
    }

    const PaDeviceInfo* deviceInfo = Pa_GetDeviceInfo(Pa_GetDefaultOutputDevice());
    std::cout << deviceInfo->name << std::endl;

    PaStream* stream;
    PaStreamParameters params;
    params.device = Pa_GetDefaultOutputDevice();
    params.suggestedLatency = deviceInfo->defaultHighOutputLatency;
    params.channelCount = playBackInfo.wavInfo->channelNum;
    params.hostApiSpecificStreamInfo = NULL;

    switch (playBackInfo.wavInfo->bytesPerSample) {
        case 1:
            params.sampleFormat = paInt8;
            break;
        case 2:
            params.sampleFormat = paInt16;
            break;
        case 3:
            params.sampleFormat = paInt24;
            break;
        default:
            params.sampleFormat = paInt16;
            break;
    }

    error = Pa_OpenStream(&stream, NULL, &params, playBackInfo.wavInfo->sampleRate, paFramesPerBufferUnspecified, 0, &portAudioPlayCallback, &playBackInfo);
    if(error != paNoError) {
        fprintf(stderr, "Pa_OpenStream failed: (err %i) %s\n", error, Pa_GetErrorText(error));
        if(stream)
            Pa_CloseStream(stream);
        return;
    }

    error = Pa_StartStream(stream);
    if (error != paNoError) {
        fprintf(stderr, "Pa_StartStream failed: (err %i) %s\n", error, Pa_GetErrorText(error));
    }

    while (Pa_IsStreamActive(stream)) {
        usleep(1000);
    }

    error = Pa_CloseStream(stream);
    if (error != paNoError) {
        fprintf(stderr, "Pa_CloseStream failed: (err %i) %s\n", error, Pa_GetErrorText(error));
    }

    Pa_Terminate();
}

int main(int argc, char** argv) {
    std::cout << "launch portaudio example!" << std::endl;
    if (argc == 1) {
        std::cout << "must specified wav file" << std::endl;
        return -1;
    }

    std::cout << "read wav from " << argv[1] <<  std::endl;

    WavFileFormat* wavInfo = loadWavData(argv[1]);

    if (wavInfo == NULL) {
        return 1;
    }

    PlayBackInfo playBackInfo;
    playBackInfo.wavInfo = wavInfo;
    playBackInfo.currentIndex = 0;

    playByPortAudio(playBackInfo);

    delete[] wavInfo->wavData;
    delete wavInfo;
    return 0;
}